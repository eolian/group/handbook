# Eolian Handbook

This document serves as a handbook for working with Eolian and on Eolian projects. Our intent is to cover patterns and practices of working across various projects, the expected roles and responsibilities, and common tooling used.